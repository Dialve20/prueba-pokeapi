import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    opendialog: false
  },
  mutations: {
    SET_DIALOG: function (state, payload) {
      state.opendialog = payload;
  },
  },
  actions: {
  },
  modules: {
  }
})
